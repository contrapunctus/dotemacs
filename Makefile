.phony: all setup tangle lint clean

all: setup tangle lint

setup:
	emacs --batch --eval="(package-initialize)" \
	--eval="(mapcar #'package-install '(indent-lint package-lint relint))"

# No -q or -Q, in case the user has a newer version of Org.
init-org.el:
	emacs --batch \
	--eval="(require 'ob-tangle)" \
	--eval='(org-babel-tangle-file "init.org")'

tangle: init-org.el

lint-check-declare: tangle
	emacs -q -Q --batch --eval='(check-declare-file "init.el")'

lint-checkdoc: tangle
	emacs -q -Q --batch --eval='(checkdoc-file "init.el")'

lint-package-lint: setup tangle
	emacs -Q --batch --eval='(package-initialize)' \
        --eval="(require 'package-lint)" \
        -f 'package-lint-batch-and-exit' "init.el"

lint-relint: setup tangle
	emacs -q -Q --batch --eval='(relint-file "init.el")'

lint: lint-check-declare lint-checkdoc lint-package-lint lint-relint

clean-tangle:
	-rm init.el
