(defvar fake-processes nil)
(defvar fake-queue nil)
(setq fake-processor
      (run-with-timer 5 5 (lambda ()
                            (when fake-processes
                              (message "Fake-Processes: %S, Fake-Queue: %S" fake-processes fake-queue)
                              (setf fake-processes (cdr fake-processes))))))

(defun fake-process-queue ()
  (when fake-queue
    (when (< (length fake-processes) 3)
      (setf fake-processes (cons (car fake-queue)
                                 fake-processes))
      (setf fake-queue (cdr fake-queue)))
    (run-with-timer 3 nil 'fake-process-queue)))

(setq fake-queue (number-sequence 1 10))

(fake-process-queue)
