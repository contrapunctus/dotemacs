(require 'helm-config)
(require 'helm-grep)
(global-set-key (kbd "C-x C-l") 'helm-mini)
(global-set-key (kbd "C-c h") 'helm-mode)
(define-key helm-map (kbd "C-h") 'backward-delete-char)
(define-key helm-map (kbd "C-w") 'backward-kill-word)
(global-set-key (kbd "M-x") 'helm-M-x)
(global-set-key (kbd "<menu>") 'helm-M-x)
;(helm-mode 1)
(global-set-key (kbd "C-c C-h") 'helm-command-prefix)

(global-set-key (kbd "C-x cl") 'count-lines-page)
(global-set-key (kbd "C-x cf") 'make-frame)

;; stop making helm tab-complete in ERC
(setq helm-mode-no-completion-in-region-in-modes 'erc-mode)
(add-to-list 'helm-mode-no-completion-in-region-in-modes 'erc-mode)
(setq helm-mode-handle-completion-in-region 'nil)
