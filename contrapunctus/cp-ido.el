(use-package ido
  :disabled
  :config (setq ido-use-virtual-buffers t
                ido-enable-flex-matching t
                ;; ido-enable-regexp nil
                ido-case-fold t
                ido-use-faces nil)
  (ido-vertical-mode 1)
  (ido-ubiquitous-mode 1)
  :bind (:map ido-common-completion-map
              ("C-n"     . ido-next-match)
              ("C-p"     . ido-prev-match)
              ("C-x C-p" . ido-toggle-prefix)
              ("M-n"     . next-history-element)
              ("M-p"     . previous-history-element)
              ("C-l"     . cp/ido-toggle-flex)))

(use-package ido-completing-read+
  :disabled
  :commands ido-ubiquitous-mode)

(defun cp/ido-toggle-flex ()
  "Toggle the value of `ido-enable-flex-matching'."
  (interactive)
  (setq flx-ido-mode
        (not flx-ido-mode)))

;; TODO - something to show documentation for a candidate, instead of
;; selecting it.
(use-package smex
  :disabled
  :bind ("M-x" . smex))

;; This creates a bug with shell completion if you're using ivy mode.
(unless (bound-and-true-p ivy-mode)
  (setq completion-styles
        '(partial-completion initials)
        completion-cycle-threshold nil))

;; is this being saved by desktop.el and actually being disabled here?
;; (toggle-uniquify-buffer-names)

(provide 'cp-ido)
