;; mouse-1, 2, 3 - the usual suspects
;; mouse-4 and 5 - wheel up and wheel down
;; mouse-9 and 8 - 'forward' and 'backward' thumb buttons

;; (require 'ediff)

;; (cp-set-keys
;;  :keymap ediff-mode-map
;;  :bindings
;;  '(([mouse-4] ediff-previous-difference)
;;    ([mouse-5] ediff-next-difference)))

;; (add-hook 'ediff-mode-hook
;;           #'(lambda ()
;;               (define-key ediff-mode-map [mouse-4] 'ediff-previous-difference)
;;               (define-key ediff-mode-map [mouse-5] 'ediff-next-difference)))

; window management (stumpwm) (written here so I know which ones I
; want to reserve for the future)
; super + mousewheel up/down - next/previous window
