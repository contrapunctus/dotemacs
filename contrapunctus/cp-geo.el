;; Support for the geo: URI scheme in Emacs

(with-eval-after-load 'markdown-mode
  (defun markdown-follow-link-at-point ()
    "Like `markdown-follow-link-at-point' (originally defined in
 `markdown-mode.el') but modified to support geo: URIs.

Open the current non-wiki link. If the link is a complete URL,
open in browser with `browse-url'. Otherwise, open with
`find-file' after stripping anchor and/or query string. Translate
filenames using `markdown-filename-translate-function'."
    (interactive)
    (if (markdown-link-p)
        (let* ((url (markdown-link-url))
               (struct (url-generic-parse-url url))
               (full (url-fullness struct))
               (file url))
          ;; Parse URL, determine fullness, strip query string
          (if (fboundp 'url-path-and-query)
              (setq file (car (url-path-and-query struct)))
            (when (and (setq file (url-filename struct))
                       (string-match "\\?" file))
              (setq file (substring file 0 (match-beginning 0)))))
          ;; Open full URLs in browser, files in Emacs
          ;; Hacky modification for geo: support -
          (if (or full (string-match-p "geo:.*" url))
              (browse-url url)
            (when (and file (> (length file) 0))
              (find-file (funcall markdown-translate-filename-function file)))))
      (user-error "Point is not at a Markdown link or URL"))))

;; in ERC
(with-eval-after-load 'erc
  (setq erc-button-url-regexp
        "\\(www\\.\\|\\(s?https?\\|ftp\\|file\\|gopher\\|news\\|telnet\\|wais\\|mailto\\|geo\\):\\)\\(//[-a-zA-Z0-9_.]+:[0-9]*\\)?[-a-zA-Z0-9_=!?#$@~`%&*+\\/:;.,()]+[-a-zA-Z0-9_=#$@~`%&*+\\/()]"))
