(require 's)
(defconst cp-comp-path-pre   "/media/alpha/kashish/")
(defconst cp-device-path-pre "/storage/sdcard0/")

(defun cp-adb-push-this-file ()
  (interactive)
  (let* ((f buffer-file-name)
         (subpath (s-chop-prefix cp-comp-path-pre f))
         (output-path (concat cp-device-path-pre subpath)))
    (start-process "adb-push" "adb-push" "adb" "push" f output-path)))

;; (defun cp-adb-pull-and-ediff ()
;;   (interactive)
;;   (let* ((f buffer-file-name)
;;          (subpath (s-chop-prefix cp-comp-path-pre f))
;;          (input-path  (concat cp-device-path-pre subpath))
;;          (output-path (concat cp-comp-path-pre
;;                               (s-replace "." "2." subpath))))
;;     ;; problem -
;;     ;; 1. ediff gets run even if adb fails.
;;     ;; 2. user does not get to know if it was successful or not.
;;     (start-process "adb-pull" "adb-pull" "adb" "pull" input-path output-path)
;;     ;; (start-process "adb-pull" "*Messages*" "adb" "pull" input-path output-path) ; advisable?
;;     ;; (sleep-for 2)
;;     ;; (format "%s" (process-status "adb-pull"))
;;     ;; (message (number-to-string (process-exit-status (get-process "adb-pull"))))

;;     ;; (if (= 0 (process-exit-status (get-process "adb-pull")))
;;     (ediff f output-path)
;;     ;;   (message "adb exited with an error."))
;;     ))

(defun cp-adb-pull-and-ediff ()
  (interactive)
  (let* ((f buffer-file-name)
         (subpath (s-chop-prefix cp-comp-path-pre f))
         (input-path  (concat cp-device-path-pre subpath))
         (output-path (concat cp-comp-path-pre
                              (s-replace "." "2." subpath))))
    (start-process "adb-pull" "adb-pull" "adb" "pull" input-path output-path)

    (ediff f output-path)))

;; extend functions to ask for paths, with path history, and default
;; path being directory of current file?
(cp-set-keys
 :bindings
 `((,(kbd "C-x C-a C-h") cp-adb-push-this-file)
   (,(kbd "C-x C-a C-d") cp-adb-pull-and-ediff)))

(defun cp-dired-adb-push-marked ()
  (interactive))
