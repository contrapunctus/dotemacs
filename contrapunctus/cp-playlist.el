(require 'launch)
(require 'whole-line-or-region)

;; salient features -
;; 1. C-k is whole-line-or-region-kill-region, not kill-line
;; 2. plm/launch-file (s-l) will launch the file in the current line
;; 3. plm/verify (C-c C-v) can check if each entry in the playlist exists

(defun plm/file-at-point ()
  (buffer-substring (point-at-bol)
                    (point-at-eol)))

;; TODO - buggy cond
(defun plm/launch-file (arg)
  (interactive "P")
  (cond
   ;; if region is active, launch each file in the region
   ;; it starts at the beginning of the region and goes to the end, so
   ;; the order of files is reversed if you are, for instance, running
   ;; something like smplayer -add-to-playlist <file> on each entry
   ((use-region-p)
    (let ((a (region-beginning))
          (b (line-number-at-pos)))
      (goto-char a)
      (beginning-of-line)
      (while (not (= (line-number-at-pos) b))
        (launch-file (plm/file-at-point))
        (forward-line))))
   ;; TODO - if
   ((null arg)
    ;; ;; old code, just plays the file at point
    (let ((f (plm/file-at-point)))
      (pcase f
        ("" (message "plm/launch-file: No file here."))
        (t  (launch-file (plm/file-at-point))))))

    ;; (async-shell-command (concat "smplayer -actions \""  "\"" (buffer-file-name)))
   ((or (numberp arg)
        (= 1 arg))
    (launch-file (buffer-file-name)))
   (t
    (call-interactively 'launch-file))))

(define-derived-mode playlist-mode text-mode "Playlist"
  "Major mode for editing playlists."
  ;; TODO - investigate why this doesn't work. (auto-fill-mode -1) is
  ;; what actually works, unlike what the docstring suggests -
  ;; (auto-fill-mode nil). Try with -q/-Q
  (auto-fill-mode -1)
  (setq truncate-lines 1)
  (turn-off-smartparens-mode)
  (define-key playlist-mode-map (kbd "C-k")     'whole-line-or-region-kill-region)
  (define-key playlist-mode-map (kbd "C-S-k")   'kill-line)
  (define-key playlist-mode-map (kbd "s-l")     'plm/launch-file)
  (define-key playlist-mode-map (kbd "C-c C-v") 'plm/verify))

(add-to-list 'auto-mode-alist (cons "\\.m3u$" 'playlist-mode))

(defun plm/verify-one-file ()
  (if (file-exists-p (plm/file-at-point))
      (forward-line)
    nil))

;; TODO - offer to fix file? (i.e. if user selects y, prompt for a file)
(defun plm/verify ()
  (interactive)
  (goto-char (point-min))
  (catch 'break
    (while (not (eobp))
      (unless (plm/verify-one-file)
        (throw 'break
               (progn
                 (message "%s" (propertize "Failure! This file is missing." 'face 'warning))
                 (recenter)
                 nil))))
    (message "%s" (propertize "Success! No missing files." 'face 'success))
    t))

(defun plm/verify-folder (path)
  (interactive)
  (let ((playlists
         (->>
          (directory-files path)
          (-filter (lambda (filename)
                     (equal "m3u"
                            (file-name-extension filename)))))))
    (catch 'break
      (dolist (elt playlists)
        (find-file elt)
        (plm/verify)))
    t))

(provide 'playlist)
