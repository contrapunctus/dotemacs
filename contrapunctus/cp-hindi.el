;; ;; 2017-10-13T23:00:20 - discovered devanagari-itrans input method
;; ;; - these do not work with that
;;
;; (cp-set-keys
;;  :bindings
;;  `((,(kbd "C-ॉ")    toggle-input-method)
;;    (,(kbd "C-ल")    next-line)
;;    (,(kbd "C-ज")    previous-line)
;;    (,(kbd "C-व")     backward-char)
;;    (,(kbd "C-ि")    forward-char)
;;    (,(kbd "C-प")     backward-delete-char)
;;    (,(kbd "C-र")     electric-newline-and-maybe-indent)
;;    (,(kbd "C-ं C-v") find-alternate-file)
;;    (,(kbd "C-ं C-l") ido-mini)
;;    (,(kbd "C-ं C-s") save-buffer)
;;    (,(kbd "C-ं C-f") find-file)
;;    (,(kbd "C-य")     undo-tree-undo)
;;    (,(kbd "C-?")     undo-tree-redo)
;;    (,(kbd "C-ं C-1") delete-other-windows)
;;    (,(kbd "C-ं C-2") split-window-below)
;;    (,(kbd "C-ं C-3") split-window-right)
;;    (,(kbd "C-ो")     beginning-of-line)
;;    (,(kbd "C-ा")     end-of-line)
;;    (,(kbd "C-ग")      indent-for-tab-command)
;;    (,(kbd "C-ह")      cp-kill-line-0)
;;    (,(kbd "C-ब")     whole-line-or-region-yank)
;;    (,(kbd "C-द")     cp/open-line-before)
;;    (,(kbd "C-ष")     beginning-of-buffer)
;;    (,(kbd "C-।")     beginning-of-buffer)
;;    (,(kbd "C-अ")    sp-delete-char)
;;    (,(kbd "C-ै")    sp-backward-kill-word)
;;    ))

;; add "।" (poorna virama) as another possible sentence end
(setq sentence-end-base "[.?!।][]\"'”)}]*")
(with-eval-after-load 'ind-util
  ;; after making such changes, re-evaluate the call to
  ;; quail-define-indian-trans-package in indian.el.gz
  ;;
  ;; or just eval -
  ;; (quail-define-indian-trans-package
  ;;  indian-dev-itrans-v5-hash "devanagari-itrans" "Devanagari" "DevIT"
  ;;  "Devanagari transliteration by ITRANS method.")

  (remhash "ऩ्" (car indian-dev-itrans-v5-hash))
  (remhash "nh" (cdr indian-dev-itrans-v5-hash))

  ;; (puthash "न्ह" "nh" (car indian-dev-itrans-v5-hash))
  ;; (puthash "zy" "ज़्य" (cdr indian-dev-itrans-v5-hash))

  (maphash (lambda (key val)
             (if (string-match-p "^ld" key)
                 (remhash key (cdr indian-dev-itrans-v5-hash))))
           (cdr indian-dev-itrans-v5-hash))
  (maphash (lambda (key val)
             (when (string-match-p "^ऩ" key)
               (remhash key (car indian-dev-itrans-v5-hash))))
           (car indian-dev-itrans-v5-hash))
  (maphash (lambda (key val)
             (when (string-match-p "^nh" key)
               (remhash key (cdr indian-dev-itrans-v5-hash))))
           (cdr indian-dev-itrans-v5-hash))
  (mapc (lambda (elt)
          (let ((lat (car elt))
                (dev (cdr elt)))
            (puthash dev lat
                     (car indian-dev-itrans-v5-hash))
            (puthash lat dev
                     (cdr indian-dev-itrans-v5-hash))))
        '(("nh"  . "न्ह्")
          ("nha" . "न्ह") ("nhaa" . "न्हा") ("nhA" . "न्हा")
          ("nhi" . "न्हि") ("nhii" . "न्ही") ("nhI" . "न्ही")
          ("nhu" . "न्हु") ("nhuu" . "न्हू") ("nhU" . "न्हू")
          ("nhe" . "न्हे") ("nhai" . "न्है")

          ("ld"  . "ल्द्")
          ("lda" . "ल्द") ("ldaa" . "ल्दा") ("ldA" . "ल्दा")
          ("ldi" . "ल्दि") ("ldii" . "ल्दी") ("ldI" . "ल्दी")
          ("ldu" . "ल्दु") ("lduu" . "ल्दू") ("ldU" . "ल्दू")
          ("lde" . "ल्दे") ("ldai" . "ल्दै"))))

;; (gethash "ऩ्" (car indian-dev-itrans-v5-hash))
;; (gethash "न्ह" (car indian-dev-itrans-v5-hash))
;; (gethash "nh" (cdr indian-dev-itrans-v5-hash))

(provide 'cp-hindi)
