(defun cp/pem-timestamp (date-time)
  "Converts user input date and time to UNIX epoch time and
inserts it after point.

A timezone must be specified, or  MUST include the timezone!"
  (interactive "sDate and time: ")
  (insert
   (format-time-string
    "%s"
    (date-to-time date-time))))
