;;;; Hacks for editing SFZ files

;; lern to syntax table
(define-derived-mode sfz-mode html-mode "SFZ"
  "Major mode for editing SFZ files."
  (highlight-lines-matching-regexp "^//" "font-lock-comment-face")
  ;; (highlight-lines-matching-regexp " [a-z]*" "font-lock-constant-face")
  (setq comment-start "//"
        comment-end "")
  (auto-fill-mode -2))
(add-to-list 'auto-mode-alist
             (cons "\\.sfz$" 'sfz-mode))

(defun sfz-prev-group ()
  (interactive)
  (re-search-backward "<group>")
  (beginning-of-line))

(defun sfz-next-group ()
  (interactive)
  (forward-char)
  (re-search-forward "<group>")
  (beginning-of-line))

(define-key sfz-mode-map (kbd "M-p") 'sfz-prev-group)
(define-key sfz-mode-map (kbd "M-n") 'sfz-next-group)

(provide 'cp-sfz)
