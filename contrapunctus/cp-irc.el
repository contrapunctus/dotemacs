;;;; IRC packages config

;; TODO - Revamp C-x e e - use hydra or magit-popup to make a menu
;; which shows servers you've set up to use. Indicate via faces or
;; symbols if you are connected to them or not. If connected,
;; selecting the server switches to its server buffer, else connects
;; to it.
;;
;; Likewise revamp C-x e r and C-x e w

(setq erc-autojoin-channels-alist
      '(("freenode"
         ;; "##archlinux-animangames"

         "#emacs"
         "##emacs.de"
         ;; "#lilypond"
         ;; "#lisp"
         ;; "#lispgames"
         ;; "#scheme"

         "#opensourcemusicians"

         "##privacy"
         "#selfhost"

         "##classical"
         ;; "##guitar"
         ;; "#music-theory"
         ;; "##piano"

         "##hindi"
         ;; "##italiano"
         "##deutsch"

         ;; "#VelvetAndLace"
         ;; "#wikisource"
         )
        ("oftc"
         ;; "#debian-next"
         "#osm"
         "#osm-de"
         "#osm-asia")
        ("mozilla"
         "#firefox")))

(with-eval-after-load 'erc
  (defun cp-erc-generate-log-file-name (buffer target nick server port)
    "Generates a log-file name using only the channel and network
names rather than server name. This results in a file name of the
form irc.NETWORK.#CHANNEL.erclog. This function is a possible
value for `erc-generate-log-file-name-function'."
    (require 'erc-networks)
    (let ((file (concat
                 ;; (replace-regexp-in-string "^.*?\\." "irc." erc-server-announced-name)
                 erc-session-server
                 ;; (erc-compute-server)

                 ;; "irc."
                 ;; (or (with-current-buffer buffer (erc-network-name)) server)
                 ;; (if target (concat target "!"))
                 "."
                 target
                 ".erclog")))
      ;; we need a make-safe-file-name function.
      (convert-standard-filename file)))

  (setq ;; erc-default-coding-system 'utf-8
   erc-password "trololo_trololo"
   erc-fill-mode nil

   erc-generate-log-file-name-function 'cp-erc-generate-log-file-name
   ;; erc-generate-log-file-name-function 'erc-generate-log-file-name-network
   erc-log-channels-directory "~/.emacs.d/erc/logs"
   erc-log-mode t
   erc-save-buffer-on-part nil
   erc-save-queries-on-quit nil
   erc-log-write-after-send t
   erc-log-write-after-insert t

   erc-modules '(autojoin completion list log menu ring stamp ercn
                          netsplit button match track readonly networks noncommands
                          irccontrols move-to-prompt)
   erc-nick '("contrapunctus"
              "contrapunctus-1"
              "contrapunctus-2"
              "contrapunctus-3"
              ;; "HisaoNakai"
              ;; "[Batou]"
              ;; "[Togusa]"
              ;; "[2501]"
              )
   erc-stamp-mode t
   erc-timestamp-format-left "[%FT%T%z]
"
   erc-timestamp-format-right nil
   erc-join-buffer 'bury
   erc-fools '("c-c@" "Kimryd" "csmr" "NeoFrontier")
   ;; erc-part-reason 'cp/erc-random-exitmsg
   ;; erc-quit-reason 'cp/erc-random-exitmsg
   erc-part-reason 'cp/quitmsg
   erc-quit-reason 'cp/quitmsg
   erc-keywords '("contrapunctus\[m\]")
   ;; 2018-07-29T11:26:01+0530 - commented out, causes too many issues if I lose connectivity
   ;; erc-server-reconnect-attempts t
   )

  (define-key erc-mode-map (kbd "C-c C-c") nil)
  (add-hook 'erc-mode-hook 'erc-fill-disable)
  (add-hook 'erc-mode-hook (lambda () (setq erc-ignore-list '("*!csmr@kapsi.fi"))))

  ;; (defun cp/erc-random-exitmsg (quit-string)
  ;;     "If the user is in more than 5 channels, return a random quit
  ;; message. QUIT-STRING (see `erc-quit-reason') is ignored."
  ;;     (if (>= 5 (length (erc-channel-list nil)))
  ;;         (cp/random-elt
  ;;          '("Interested in making music with libre/open-source tools? Feel free to join #opensourcemusicians!"
  ;;            "Love classical music? Join ##classical!"
  ;;            "Check out Freenode's bicycles group - ##bicycles !"
  ;;            ))
  ;;       "ciao"))
  (defun cp/quitmsg (quit-string)
    (cp/random-elt
     '(
       "\"The Eternal Value of Privacy\", Bruce Schneier - https://www.schneier.com/essays/archives/2006/05/the_eternal_value_of.html"
       "Take back your right to privacy. https://prism-break.org/"
       "Help make mass surveillance of entire populations uneconomical. https://prism-break.org/"
       "Why privacy is important, and having \"nothing to hide\" is irrelevant. https://robindoherty.com/2016/01/06/nothing-to-hide.html"
       "Interested in making music with freedom-respecting tools? Feel free to join #opensourcemusicians!"
       "Love classical music? Join us in ##classical!"
       "https://medium.freecodecamp.com/ill-never-bring-my-phone-on-an-international-flight-again-neither-should-you-e9289cde0e5f#.58zrgizib"
       "Vote with your wallet for FOSS - buy hardware which supports free software. https://h-node.org/home/index/en"
       "Contribute to H-Node and help people vote with their wallets for FOSS! https://h-node.org/home/index/en"
       ;; make this appear (only in #emacs|if the only channel is #emacs)?
       "German-speaking Emacs users! Come lurk in ##emacs.de!"
       )))

  ;; 2018-08-25T23:34:24+0530
  ;; a more interesting and complicated refinement would be to be able
  ;; to access earlier history by just scrolling up in the ERC buffer.
  (defun cp/erc-open-corresponding-log-file ()
    (interactive)
    (find-file
     (concat erc-log-channels-directory
             "/"
             (apply erc-generate-log-file-name-function
                    (current-buffer)
                    (erc-default-target)
                    (erc-compute-nick)
                    erc-session-server
                    erc-default-port
                    nil))))
  (define-key erc-mode-map (kbd "C-c C-f") 'cp/erc-open-corresponding-log-file))

(autoload 'erc "erc" "ERC." t)
;; erc-server-buffer-p is the first ERC function called by
;; cp-erc-freenode, which is what I use to start ERC
(autoload 'erc-server-buffer-p "erc" "ERC." t)

;; (let ((results (remove-if-not (lambda (a)
;;                                   (string-match regexp a))
;;                                 (mapcar #'buffer-name (buffer-list)))))
;;   (if all results (car results)))
;; (defun cp-erc-server-buffer ()
;;   (cp-get-buffer-regexp "^irc"))

(defun cp-erc-server-buffer (&optional all)
  (let ((results (remove-if-not #'erc-server-buffer-p
                                (buffer-list))))
    (if all results (car results))))
(defun cp-erc-freenode ()
  (interactive)
  (let ((bufname (cp-erc-server-buffer)))
    (if bufname
        (switch-to-buffer bufname)
      (erc :server "irc.freenode.net"
           :nick "contrapunctus"
           :password "trololo_trololo"))))
(defun cp-erc-reconnect ()
  (interactive)
  (if (equal major-mode 'erc-mode)
      (erc-server-reconnect)
    (progn (switch-to-buffer (cp-erc-server-buffer))
           (erc-server-reconnect))))
(defun cp-erc-ghost ()
  (interactive)
  ;; (cp-erc-server-buffer)
  (erc-message "PRIVMSG"
               (if (string-match-p "freenode" erc-server-announced-name)
                   "NickServ GHOST contrapunctus"
                 "NickServ REGAIN contrapunctus trololo"))
  (erc-cmd-NICK "contrapunctus"))
(defun cp-erc-toggle-autojoin ()
  (interactive)
  (if erc-autojoin-mode
      (progn (erc-autojoin-disable)
             (message "ERC - autojoin disabled."))
    (progn (erc-autojoin-enable)
           (message "ERC - autojoin enabled."))))
;; (defun cp-erc-cycle ()
;;   (interactive)
;;   ())

(global-unset-key (kbd "C-x e"))
(cp-set-keys
 :bindings
 `((,(kbd "C-x e r") cp-erc-reconnect)
   (,(kbd "C-x e e") cp-erc-freenode)
   (,(kbd "C-x e w") cp-erc-ghost)
   (,(kbd "C-c C-c") erc-track-switch-buffer)
   (,(kbd "C-x e a") cp-erc-toggle-autojoin)))

(define-derived-mode erc-log-read-mode text-mode "ERC-log"
  "Major mode for viewing ERC logs."
  ;; (highlight-lines-matching-regexp "^\[[0-9: ]*\]$"
  ;;                                  "font-lock-comment-face")
  ;; (highlight-lines-matching-regexp "^\*\*\*.*\(join\|quit\|part\)"
  ;;                                  "font-lock-comment-face")
  (setq buffer-read-only t)
  )
;; (add-to-list 'auto-mode-alist
;;              (cons "/erc/logs/" 'erc-log-read-mode))
(add-to-list 'auto-mode-alist
             (cons ".erclog$" 'erc-log-read-mode))

(with-eval-after-load 'rcirc
  (setq
   rcirc-log-flag t
   rcirc-time-format "%Y %m %e %T "
   rcirc-fill-column 80
   rcirc-server-alist '(("irc.freenode.net"
                         :channels
                         ("#emacs"
                          "#lilypond"
                          "#velvetandlace"
                          "#opensourcemusicians"
                          "##classical"
                          "##guitar"
                          "##deutsch"
                          "##hindi")
                         :password
                         "trololo_trololo"))
   rcirc-default-nick "contrapunctus"
   ;; rcirc-coding-system-alist '(("" . 'utf-8))
   ))

;; TODO
;; open log file for current channel

;; 2018-08-01T00:28:35+0530
;; https://www.emacswiki.org/emacs/ErcIgnoring
(defcustom erc-ignore-content-regexps
  '(
    "so, he wants the win. so we're just gonna get lunch or something, then hes gonna push me to the ground and tap my ass with his foot so he can claim he \"kicked my ass\" tbh im going along with it becase I dont wanna lose any teeth."
    "With our IRC ad service you can reach a global audience of entrepreneurs and fentanyl addicts with extraordinary engagement rates! https://williampitcock.com/"
    )
  "Regular expressions to identify content to ignore."
  :group 'erc
  :type '(repeat regexp))

(defun erc-content-ignore-p (msg)
  "Check whether MSG is to be ignored"
  (->> (mapcar (lambda (regexp)
                 (string-match-p regexp msg))
               erc-ignore-content-regexps)
       (-any? #'identity)))

(defun erc-ignore-message-by-content (message)
  (when (erc-content-ignore-p message)
    (setq erc-insert-this nil)
    (message "Ignored message - %s" message)))

(add-hook 'erc-insert-pre-hook #'erc-ignore-message-by-content)
